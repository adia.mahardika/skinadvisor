export const insertImageUrl = (data) => {
    return {
      type: "INSERT_IMAGE_URL",
      payload: data,
    };
  };