import axios from "axios";

const data = {
  name: "Bayu Idham",
  product: [
    {
      id: 69,
      qty: 2,
    },
    {
      id: 78,
      qty: 4,
    },
    {
      id: 57,
      qty: 1,
    },
  ],
};
const JSONtoBase64 = (jsonObj) => {
  return Crypto.util.bytesToBase64(
    Crypto.charenc.UTF8.stringToBytes(Object.toJSON(jsonObj))
  );
};

let encode = JSONtoBase64(data);
export const readHome = () => {
  return {
    payload: axios({
      method: "POST",
      url: `https://avoskin.demoapp.xyz/skin-advisor-checkout/?data=${encode}`,
    }),
  };
};
