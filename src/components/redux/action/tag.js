import axios from "axios";

export const getTag = (requestBody) => {
  return new Promise((resolve, reject) => {
    axios
      .post(
        "https://gserver1.btbp.org/deeptag/AppService.svc/getTags",
        requestBody,
        {
          headers: {
            Accept: "application/json",
            "Content-type": "application/json",
          },
        }
      )
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        console.log("false", error);
        reject(error);
      });
  });
};

export const insertSendTagData = (data) => {
  return {
    type: "INSERT_SEND_TAG_DATA",
    payload: data,
  };
};
