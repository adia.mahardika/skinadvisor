const INITIAL_STATE = {
  sendTagData: null,
  tags: null,
};

const Tag = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "INSERT_SEND_TAG_DATA":
      return {
        ...state,
        sendTagData: action.payload.data,
        tags: action.payload.tags,
        loading: false,
      };
    default:
      return state;
  }
};

export default Tag;
