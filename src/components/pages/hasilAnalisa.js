import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { readHasil } from "../redux/action/kondisiKulit";
import { readHome } from "../redux/action/home";
import kulitBerminyak from "../../assets/kulit-berminyak.svg";
const HasilAnalisa = () => {
  const imageUrl = useSelector((state) => state.image.data);
  const hasilKondisiKulit = useSelector(
    (state) => state.kondisiKulit.hasilKondisiKulit
  );
  const hasilMasalah = useSelector((state) => state.kondisiKulit.hasilMasalah);
  const home = useSelector((state) => state.home.home);
  const [activeAnalisa, setActiveAnalisa] = useState({});

  const dispatch = useDispatch();
  useEffect(() => {
    setActiveAnalisa(hasilKondisiKulit[0]);
    dispatch(readHasil());
    dispatch(readHome());
  }, [hasilKondisiKulit]);
  return (
    <>
      <div className="full-page-wrapper hasil-analisa-wrapper">
        <div className="text-align-center hasil-analisa-left-wrapper">
          {home && (
            <>
              <div className="belleza regular size-60 text-green">
                {home[0].title}
              </div>
              <div className="nunito light size-20 text-green mb-25">
                {home[0].subtitle}
              </div>
            </>
          )}
          <img
            style={{
              borderRadius: "10px",
              maxWidth: "25vw",
              maxHeight: "70vh",
            }}
            src={imageUrl}
            alt="Hasil Analisa"
          />
        </div>
        <div className="text-align-center hasil-analisa-right-wrapper">
          <div className="top-hasil-analisa-right-wrapper">
            <div className="belleza regular size-60 text-green">
              Hasil Analisa
            </div>
            <div className="nunito light size-20 text-green mb-25">
              Yuk kita lihat problem kulitmu dan kita lihat produk yang tepat
              untukmu
            </div>
            <img
              style={{ borderRadius: "10px", maxWidth: "75vw" }}
              src={imageUrl}
              alt="Hasil Analisa"
              className="hasil-analisa-image"
            />
          </div>
          <div className="card hasil-analisa-card-wrapper p-0">
            <div className="list-hasil-analisa-card-wrapper">
              {hasilKondisiKulit.map((value) => {
                return (
                  <button
                    key={value.id}
                    className={`${
                      activeAnalisa.name === value.name && `active`
                    } card align-items-center d-inline-grid nunito regular size-16 p-8`}
                    style={{ gridTemplateColumns: "max-content 1fr" }}
                    onClick={() => setActiveAnalisa(value)}
                  >
                    <img width="40px" src={value.image} alt={value.name} />
                    <div className="justify-self-flex-start text-green">
                      {value.name}
                    </div>
                  </button>
                );
              })}
              {hasilMasalah.map((value) => {
                return (
                  <button
                    className={`${
                      activeAnalisa.name === value.name && `active`
                    } card align-items-center d-inline-grid nunito regular size-16 p-8`}
                    style={{ gridTemplateColumns: "max-content 1fr" }}
                    onClick={() => setActiveAnalisa(value)}
                  >
                    <img width="40px" src={value.image} alt={value.name} />
                    <div className="justify-self-flex-start text-green">
                      {value.name}
                    </div>
                  </button>
                );
              })}
            </div>
            <div className="description-hasil-analisa-card-wrapper">
              <div className="nunito bold size-20 text-green">
                {activeAnalisa && activeAnalisa.name}
              </div>
              <div className="nunito light size-16 line-height-32">
                {activeAnalisa && activeAnalisa.description}
              </div>
              <div className="d-inline-grid align-self-flex-end">
                <Link to="/analisa-kulit" className="justify-self-center">
                  <button className="btn button-white nunito bold size-14 px-32 text-green text-align-left">
                    <ion-icon name="arrow-back"></ion-icon>
                    &ensp;Kembali Analisa Kulit
                  </button>
                </Link>
                <Link to="/rekomendasi-produk" className="justify-self-center">
                  <button className="btn button-green nunito bold size-14 px-32 text-green text-align-left">
                    Rekomendasi Produk&ensp;
                    <ion-icon name="arrow-forward"></ion-icon>
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default HasilAnalisa;
