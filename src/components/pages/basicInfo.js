import React, { useState, useEffect } from "react";
import imageWoman from "../../assets/woman-2.png";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { insertUserData } from "../redux/action/user";
import { readBasicInfo } from "../redux/action/basicInfo";
import Moment from "moment";
const BasicInfo = () => {
  const dipatch = useDispatch();
  const [userData, setUserData] = useState(null);
  const basic_info = useSelector((state) => state.basicInfo.basic_info);

  let today = new Date().toISOString().substr(0, 10);

  const onChangeUserData = (event) => {
    setUserData({
      ...userData,
      [event.target.name]: event.target.value,
    });
  };

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(readBasicInfo());
  }, []);

  return (
    <>
      <div className="layout-1">
        <div className="left-wrapper px-64 pt-32">
          {basic_info && (
            <>
              <div className="belleza regular size-60 text-dark-green line-height-85percent">
                {basic_info[0].title}
              </div>
              <div
                style={{ alignSelf: "flex-start" }}
                className="nunito regulat size-24 text-green"
              >
                {basic_info[0].subtitle}
              </div>
            </>
          )}
          <img
            className="heigth-500px width-90vw"
            src={basic_info && basic_info[0].image}
            alt="Skin Advisor"
          ></img>
        </div>
        <div className="right-wrapper px-16 py-64 p-32">
          <ul className="steps nunito regular text-disabled size-14">
            <div className="text-green d-flex align-items-center justify-content-flex-end">
              <ion-icon name="ellipse"></ion-icon>---------
            </div>
            <div className="d-flex align-items-center justify-content-center">
              <div className="text-green">--------</div>
              <ion-icon name="ellipse"></ion-icon>--------
            </div>
            <div className="d-flex align-items-center">
              ---------<ion-icon name="ellipse"></ion-icon>
            </div>
            <div className="text-green d-flex align-items-center justify-content-center">
              Basic Info
            </div>
            <div className="d-flex align-items-center justify-content-center">
              Info Kondisi Kulit
            </div>
            <div className="d-flex align-items-center justify-content-center">
              Analisa Kulit
            </div>
          </ul>
          <div
            className="card align-self-flex-start p-64"
            style={{ justifyItems: "normal" }}
          >
            <div className="nunito regular size-20 text-align-center">
              Basic Information Form
            </div>
            <div className="form-group">
              <div className="nunito regular size-14">
                Hai Sahabat Avo, kenalan dulu yuk!
              </div>
              <input
                className="form-control width-400px text-align-center"
                type="text"
                placeholder="Nama Kamu"
                name="name"
                onChange={onChangeUserData}
              />
            </div>
            <div className="form-group">
              <div className="nunito regular size-14">
                Kapan tanggal lahirmu?
              </div>
              <div
                className="input-group date width-400px position-relative"
                id="datepicker1"
              >
                <input
                  id="birth_date"
                  type="date"
                  className="form-control text-align-center"
                  name="birth_date"
                  max={today}
                  onChange={(event) => {
                    setUserData({
                      ...userData,
                      birth_date: Moment(event.target.value).format(
                        "DD-MM-YYYY"
                      ),
                    });
                  }}
                />
                <label
                  className="input-group-addon position-absolute p-4px"
                  htmlFor="birth_date"
                  style={{ right: "0", zIndex: "10" }}
                >
                  <ion-icon
                    style={{ fontSize: "28px" }}
                    name="calendar"
                  ></ion-icon>
                </label>
              </div>
            </div>
            <div className="form-group width-400px">
              <div className="nunito regular size-14">Jenis Kelamin</div>
              <div className="custom-control custom-radio custom-control-inline">
                <input
                  type="radio"
                  id="customRadioInline1"
                  name="gender"
                  className="custom-control-input"
                  onChange={() => setUserData({ ...userData, gender: "Woman" })}
                />
                <label
                  className="custom-control-label nunito regular size-14"
                  htmlFor="customRadioInline1"
                >
                  Perempuan
                </label>
              </div>
              <div className="custom-control custom-radio custom-control-inline">
                <input
                  type="radio"
                  id="customRadioInline2"
                  name="gender"
                  className="custom-control-input"
                  onChange={() => setUserData({ ...userData, gender: "Man" })}
                />
                <label
                  className="custom-control-label nunito regular size-14"
                  htmlFor="customRadioInline2"
                >
                  Laki - laki
                </label>
              </div>
              <div className="custom-control custom-radio custom-control-inline">
                <input
                  type="radio"
                  id="customRadioInline3"
                  name="gender"
                  className="custom-control-input"
                  onChange={() =>
                    setUserData({ ...userData, gender: "Unknown" })
                  }
                />
                <label
                  className="custom-control-label nunito regular size-14"
                  htmlFor="customRadioInline3"
                >
                  Tidak ingin menjawab
                </label>
              </div>
            </div>
            {userData === null || Object.keys(userData).length < 3 ? (
              <button
                className="btn button-green nunito bold size-14 px-32 justify-self-flex-end"
                disabled
              >
                Selanjutnya&ensp;<ion-icon name="arrow-forward"></ion-icon>
              </button>
            ) : (
              <Link to="/kondisi-kulit" className="justify-self-flex-end">
                <button
                  className="btn button-green nunito bold size-14 px-32"
                  onClick={() => dipatch(insertUserData(userData))}
                >
                  Selanjutnya&ensp;<ion-icon name="arrow-forward"></ion-icon>
                </button>
              </Link>
            )}
          </div>
        </div>
      </div>
    </>
  );
};
export default BasicInfo;
