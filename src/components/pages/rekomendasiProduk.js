import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { addQuantity, reduceQuantity } from "../redux/action/cart";
import cart from "../../assets/icon/cart.svg";
import { readHasil } from "../redux/action/kondisiKulit";
const RekomendasiProduk = () => {
  const total_quantity = useSelector((state) => state.cart.total_quantity);
  const hasilKondisiKulit = useSelector(
    (state) => state.kondisiKulit.hasilKondisiKulit
  );
  const hasilAlergi = useSelector((state) => state.kondisiKulit.hasilAlergi);
  const hasilMasalah = useSelector((state) => state.kondisiKulit.hasilMasalah);
  const dispatch = useDispatch();
  const [productList, setProductList] = useState([]);

  useEffect(async () => {
    dispatch(readHasil());
    let productAlergiData = [];
    let productData = [];
    await hasilAlergi.map(async (value) => {
      await value.id_product.map((idProductValue) => {
        return productAlergiData.push(idProductValue);
      });
    });
    await hasilKondisiKulit.map((value) => {
      value.product.map((valueProduct) => {
        let findProduct = productData.findIndex(
          (productValue) => productValue.id === valueProduct.id
        );
        let findAlergiProduct = productAlergiData.findIndex(
          (productAlergiValue) => productAlergiValue === valueProduct.id
        );
        if (findProduct === -1 && findAlergiProduct === -1) {
          return productData.push(valueProduct);
        }
      });
    });
    await hasilMasalah.map((value) => {
      value.product.map((valueProduct) => {
        let findProduct = productData.findIndex(
          (productValue) => productValue.id === valueProduct.id
        );
        let findAlergiProduct = productAlergiData.findIndex(
          (productAlergiValue) => productAlergiValue === valueProduct.id
        );
        if (findProduct === -1 && findAlergiProduct === -1) {
          return productData.push(valueProduct);
        }
      });
    });
    setProductList(productData);
  }, []);
  return (
    <>
      <div className="full-page-wrapper px-64 py-32">
        <div className="card p-32 rekomendasi-product-card">
          <div className="rekomendasi-produk-header-wrapper">
            <Link
              to="/hasil-analisa"
              className="justify-self-flex-start back-button"
            >
              <button className="btn button-outline-green button-pill button-circle nunito bold size-14 text-green">
                <ion-icon name="arrow-back"></ion-icon>
              </button>
            </Link>
            <div className="text-dark-green text-align-center justify-self-center">
              <div className="belleza regular size-60">Rekomendasi Produk</div>
              <div className="nunito light size-20">
                Kamu bisa gunakan produk rekomendasi berikut untuk mengatasi
                masalah kulitmu
              </div>
            </div>
          </div>
          <div className="rekomendasi-all-product-wrapper">
            {productList.map((value) => {
              return (
                <div className="product-wrapper" key={value.id}>
                  <img
                    className="product-image"
                    src={value.thumbnail}
                    alt={value.title}
                  />
                  <div className="nunito light size-17">{value.title}</div>
                  <div className="align-items-center product-button-wrapper">
                    <button
                      disabled
                      className="btn button-outline-green button-pill nunito regular size-18 text-green"
                    >
                      {value.cart_quantity}
                    </button>
                    {value.cart_quantity <= 0 ? (
                      <button className="btn p-0" disabled>
                        <ion-icon
                          style={{ color: "#8AA385" }}
                          size="large"
                          name="remove-circle"
                        ></ion-icon>
                      </button>
                    ) : (
                      <button
                        className="btn p-0"
                        onClick={() => dispatch(reduceQuantity(value))}
                      >
                        <ion-icon
                          style={{ color: "#8AA385" }}
                          size="large"
                          name="remove-circle"
                        ></ion-icon>
                      </button>
                    )}
                    {value.stock > value.cart_quantity ? (
                      <button
                        className="btn p-0"
                        onClick={() => dispatch(addQuantity(value))}
                      >
                        <ion-icon
                          style={{ color: "#8AA385" }}
                          size="large"
                          name="add-circle"
                        ></ion-icon>
                      </button>
                    ) : (
                      <button className="btn p-0" disabled>
                        <ion-icon
                          style={{ color: "#8AA385" }}
                          size="large"
                          name="add-circle"
                        ></ion-icon>
                      </button>
                    )}
                  </div>
                </div>
              );
            })}
          </div>
          <Link
            to="/keranjang-belanja"
            className="justify-self-flex-end cart-button"
          >
            <div className="d-grid p-4px" style={{ position: "relative" }}>
              <div className="nunito regular size-15 button-red button-pill">
                {total_quantity}
              </div>
              <img width="60px" height="60px" src={cart} alt="Cart" />
            </div>
          </Link>
        </div>
      </div>
    </>
  );
};

export default RekomendasiProduk;
