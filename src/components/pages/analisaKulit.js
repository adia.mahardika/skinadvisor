import React, { useState, useCallback, useRef, useEffect } from "react";
import { insertImageUrl } from "../redux/action/image";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { readHome } from "../redux/action/home";
import { getTag } from "../redux/action/tag";
import Webcam from "react-webcam";
import arrowUp from "../../assets/icon/arrow-up.svg";
import replay from "../../assets/icon/replay.svg";
import submit from "../../assets/icon/paper-airplane.svg";
import buttonCircle from "../../assets/button/button-circle.svg";
import buttonCircleMuted from "../../assets/button/button-circle-muted.svg";
import photoDefault from "../../assets/photo-default.png";
import UnggahFoto from "../../assets/icon/unggah-foto.svg";
const AnalisaKulit = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const home = useSelector((state) => state.home.home);
  const tags = useSelector((state) => state.tag.tags);
  const [pageView, setPageView] = useState(true);
  const [imageView, setImageView] = useState("default");
  const [nameView, setNameView] = useState("webcam");
  const [scanner, setScanner] = useState(false);
  const [screenWidth, setScreenWidth] = useState(null);
  const [screenHeight, setScreenHeight] = useState(null);

  const [imageUrl, setImageUrl] = useState(null);
  const [imageData, setImageData] = useState(null);
  const [imageName, setImageName] = useState(null);
  const videoConstraints = {
    width: screenWidth,
    height: screenHeight,
    facingMode: "user",
  };
  const webcamRef = useRef(null);

  const urltoFile = (url, filename, mimeType) => {
    mimeType = mimeType || (url.match(/^data:([^;]+);/) || "")[1];
    return fetch(url)
      .then(function (res) {
        return res.arrayBuffer();
      })
      .then(function (buf) {
        return new File([buf], filename, { type: mimeType });
      });
  };

  const capture = useCallback(async () => {
    const imageSrc = webcamRef.current.getScreenshot();
    const blob = await fetch(imageSrc).then((res) => res.blob());
    const formData = new FormData();
    formData.append("capture.jpeg", blob);
    setImageUrl(imageSrc);
    setImageData(formData);
    setImageView("image");
    setImageName("capture.jpeg");
    setNameView("capture");
  }, [webcamRef, setImageUrl]);

  const onInsertImage = async (event) => {
    const image = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      if (reader.readyState === 2) {
        setImageUrl(reader.result);
        setImageView("image");
        setImageName(image.name);
        setNameView("upload image");
        setImageData(image);
      }
    };
    reader.readAsDataURL(image);
  };

  useEffect(async () => {
    dispatch(readHome());

    let sW = window.screen.width;
    let sH = window.screen.height;
    if (sW <= 600) {
      setScreenWidth(sW);
      setScreenHeight(sH);
    } else {
      setScreenWidth("100%");
      setScreenHeight("100%");
    }
  }, []);

  const kirimkan = () => {
    setScanner(true);
    processTag(imageData, tags);
    setTimeout(() => {
      history.push("/hasil-analisa");
    }, 4000);
  };

  const processTag = async (base64, imageTag) => {
    await imageTag.map((value) => {
      const requestBody = {
        APIKey: "D2TS-ZIH5-VKC4-IQBX",
        Tags: value,
        ImageBytes: [base64],
      };

      // console.log("request", requestBody);
      getTag(requestBody)
        .then((result) => {
          console.log("Result tag", result);
          imageTagSuccessCallback(result.data);
        })
        .catch((error) => {
          console.log("Error tag", error);
          alert("Error get image tag from BTBP");
        });
    });
  };

  const imageTagSuccessCallback = (tagResponse) => {
    let isCallSuccess = tagResponse.Tags.every((tag) => {
      return tag.StatusCode === 200;
    });

    if (isCallSuccess) {
      console.log("Success", tagResponse);
    } else {
      for (let tag of tagResponse.Tags) {
        if (tag.StatusCode !== 200) {
          alert(tag.Message);
          return;
        }
      }
    }
  };
  console.log(imageData);
  return (
    <>
      {pageView === true && (
        <div className="analisa-kulit-wrapper">
          <div className="card text-align-center width-550px analisa-kulit-introduce">
            <div className="nunito regular size-24">Analisa Kulit</div>
            <div className="nunito regular size-14">
              Untuk menganalisa masalah kulitmu lebih lanjut, unggah fotomu
              dengan klik link berikut ini!
            </div>
            <img height="180px" src={UnggahFoto} alt="Uggah Foto"></img>
            <div className="nunito regular size-14 text-green">
              *Foto kulit wajahmu tanpa makeup untuk hasil yang lebih akurat dan
              bila kamu menggunakan kacamata, lepas terlebih dulu.
            </div>
            <button
              onClick={() => setPageView(false)}
              className="btn button-green nunito bold size-14 px-32"
            >
              Mulai Sekarang
            </button>
          </div>
        </div>
      )}
      <div
        className={`full-page-wrapper px-64 ${pageView === true && `blur-10`} ${
          screenWidth < 600 && `position-relative`
        }`}
        style={{ overflow: "hidden" }}
      >
        <div
          className={`d-grid justify-self-center image-analisa-kulit position-relative 
          `}
        >
          {imageView === "default" ? (
            <Webcam
              videoConstraints={videoConstraints}
              audio={true}
              screenshotFormat="image/jpeg"
              ref={webcamRef}
              className="justify-self-center webcam"
            />
          ) : scanner === false ? (
            <>
              <div className="d-grid image-capture-wrapper">
                <div className="nunito bold size-14 button-black button-pill px-32 py-8 image-name">
                  {imageName}
                </div>
                <img
                  className="justify-self-center image-capture"
                  src={imageUrl}
                  alt="Analisa Kulit Image"
                />
              </div>
            </>
          ) : (
            <>
              <div className="anim-box">
                <div className="scanner"></div>
                <img
                  className="justify-self-center image-capture"
                  style={{
                    opacity: "0.4",
                  }}
                  src={imageUrl}
                  alt="Analisa Kulit Image"
                />
              </div>
            </>
          )}
        </div>
        <div className="bottom-card-container">
          <div className="bottom-card-wrapper">
            <div className="text-white text-align-center bottom-card-title-mobile">
              <div className="belleza regular size-24">
                {home && home[0].title}
              </div>
              <div className="nunito light size-8">
                {home && home[0].subtitle}
              </div>
            </div>
            {imageView === "image" && (
              <div className="nunito bold button-black button-pill size-14 image-name-mobile">
                {imageName}
              </div>
            )}
            <div className="bottom-card px-32">
              <div className="text-green text-align-center bottom-card-title">
                <div className="belleza regular size-33">
                  {home && home[0].title}
                </div>
                <div className="nunito light size-10">
                  {home && home[0].subtitle}
                </div>
              </div>
              <div
                className={`${
                  scanner === false ? `d-inline-grid` : `gc-1-3`
                } align-items-center capture-wrapper`}
              >
                {scanner === false ? (
                  <>
                    <input
                      type="file"
                      id="upload-analisa-kulit"
                      accept="image/*"
                      onChange={onInsertImage}
                    />
                    <div className="d-grid justify-content-center">
                      <label
                        htmlFor="upload-analisa-kulit"
                        className="btn button-green button-pill nunito bold size-14 button-analisa-kulit unggah"
                      >
                        <img height="14px" src={arrowUp} alt="Unggah Foto" />
                        <label className="button-text-analisa-kulit">
                          Unggah Foto
                        </label>
                      </label>
                      <div className="nunito bold size-12 button-sub-text text-green">
                        Upload
                      </div>
                    </div>
                    {nameView === "webcam" ? (
                      <button className={`btn capture`} onClick={capture}>
                        <img
                          className="justify-self-center button-capture"
                          src={buttonCircle}
                          alt="Button Capture"
                        />
                      </button>
                    ) : (
                      <div
                        className={`btn capture`}
                        style={{ cursor: "default" }}
                      >
                        <img
                          className="justify-self-center button-capture"
                          src={buttonCircleMuted}
                          alt="Button Capture"
                        />
                      </div>
                    )}
                    <div className="d-grid justify-content-center">
                      <button
                        onClick={() => {
                          setImageView("default");
                          setNameView("webcam");
                          setImageUrl(null);
                        }}
                        className="button-analisa-kulit justify-self-flex-start btn button-green button-pill nunito bold size-14"
                      >
                        <img height="14px" src={replay} alt="Ulangi" />
                        <label className="button-text-analisa-kulit">
                          Ulangi
                        </label>
                      </button>
                      <div className="nunito bold size-12 button-sub-text text-green">
                        Restart
                      </div>
                    </div>
                  </>
                ) : (
                  <div className="analisa-kulit-loading-bar">
                    <div
                      className="nunito regular size-20"
                      style={{ marginBottom: "8px" }}
                    >
                      Sedang Menganalisa Fotomu..
                    </div>
                    <div className="progress">
                      <div
                        className="progress-bar progress-bar-danger"
                        style={{ width: "100%" }}
                      ></div>
                    </div>
                  </div>
                )}
              </div>
              {imageUrl === null ? (
                <div
                  className={`${
                    scanner === false ? `d-grid` : `d-none`
                  } justify-content-center`}
                >
                  <button
                    className="btn button-white nunito bold size-14 text-green button-analisa-kulit button-submit"
                    disabled
                  >
                    <div className="button-text-analisa-kulit">
                      Kirimkan&ensp;
                      <ion-icon name="arrow-forward"></ion-icon>
                    </div>
                    <img
                      height="14px"
                      src={submit}
                      alt="Unggah Foto"
                      className="paper-plane-icon"
                    />
                  </button>
                  <div className="nunito bold size-12 button-sub-text text-disabled">
                    Submit
                  </div>
                </div>
              ) : (
                <div
                  className={`${
                    scanner === false ? `d-grid` : `d-none`
                  } justify-content-center`}
                >
                  <button
                    className="btn button-white nunito bold size-14 text-green button-analisa-kulit button-submit"
                    onClick={() => {
                      kirimkan();
                      dispatch(insertImageUrl(imageUrl));
                    }}
                  >
                    <div className="button-text-analisa-kulit">
                      Kirimkan&ensp;
                      <ion-icon name="arrow-forward"></ion-icon>
                    </div>
                    <img
                      height="14px"
                      src={submit}
                      alt="Unggah Foto"
                      className="paper-plane-icon"
                    />
                  </button>
                  <div
                    className="nunito bold size-12 button-sub-text"
                    style={{ color: "white" }}
                  >
                    Submit
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
export default AnalisaKulit;
